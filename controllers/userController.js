const User = require("../models/user");
const bcrypt = require("bcryptjs");

const getUser = async (req, res) => {
    let user;
    try {
        user = await User.findOne({ _id: req.user._id });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
        return;
    }
    res.status(200).json({
        user: {
            _id: user._id,
            username: user.username,
            createdDate: user.createdDate,
        },
    });
};

const deleteUser = async (req, res) => {
    try {
        await User.deleteOne({ _id: req.user._id });
        res.status(200).json({
            message: "Success",
        });
    } catch (e) {
        res.status(500).json({
            message: e.message,
        });
    }
};

const changePassword = async (req, res) => {
    try {
        const { oldPassword, newPassword } = req.body;
        const user = await User.findOne({ _id: req.user._id });
        if (!user) {
            res.status(500).json({
                message: "User not found",
            });
        }
        const checkOldPassword = await bcrypt.compare(
            oldPassword,
            user.password
        );
        if (!checkOldPassword) {
            res.status(400).json({
                message: "Wrong password",
            });
        }
        user.password = await bcrypt.hash(newPassword, 12);
        try {
            await user.save();
        } catch(e) {
            console.log(e);
        }

        res.status(200).json({
            message: "Password changed successfully",
        });
    } catch (e) {
        res.status(500).json({
            message: "Something gone wrong, try again",
        });
    }
};

module.exports = { getUser, deleteUser, changePassword };
