const { Schema, model } = require('mongoose')

const noteSchema = new Schema({
  text: {
    type: String,
    required: true
  },
  userId: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    required: false,
    default: false
  },
  createdDate: {
    type: String,
    default: new Date().toLocaleString()
  }
});

const Note = model('notes', noteSchema);

module.exports = Note;
