const express = require('express');
const {getUser, deleteUser, changePassword} = require('../controllers/userController');
const userRouter = express.Router();
const middleware = require('../middleware/middlewares');

userRouter.get('/me', middleware, getUser);

userRouter.delete('/me', middleware, deleteUser);

userRouter.patch('/me', middleware, changePassword)

module.exports = userRouter;
