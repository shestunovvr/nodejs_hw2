require('dotenv').config()
const express = require('express');
const mongoose = require("mongoose");
const morgan = require("morgan");
const cors = require("cors");
const fs = require('fs');
const path = require('path');
const authRouter = require('./routers/authRouter'); 
const userRouter = require('./routers/userRouter');
const notesRouter = require('./routers/notesRoute');
const middleware = require('./middleware/middlewares');

const app =express();

const PORT = process.env.PORT || 8080;
const MONGO_PATH = process.env.mongoUri;

const loggerFile = path.join(__dirname, 'logs.log');
try {
  if (!fs.existsSync(loggerFile)) {
    fs.appendFileSync(loggerFile, '', 'utf8');
  }
} catch (error) {
  console.log(error);
}

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.log'), {
  flags: 'a',
});

app.use(morgan('combined', {stream: accessLogStream}));

app.use(cors());

app.use(express.json({extended: true}));

app.use("/api/users", middleware, userRouter);
app.use("/api/auth", authRouter);
app.use("/api/notes", middleware, notesRouter);

(async () => {
  try {
    await mongoose.connect(MONGO_PATH, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    app.listen(8080);
  } catch (err) {
    console.error(`Failed to start server: ${err.message}`);
    return;
  }
  console.log(`server is running at port ${PORT}`);
})();
