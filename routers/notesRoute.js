const express = require('express');
const notesController = require('../controllers/notesController');
const middleware = require('../middleware/middlewares');

const notesRouter = express.Router();

notesRouter.get('/', middleware, notesController.getNotes);
notesRouter.post('/', middleware, notesController.createNote);
notesRouter.get('/:id', middleware, notesController.getNote);
notesRouter.patch('/:id', middleware, notesController.checkNote);
notesRouter.delete('/:id', middleware, notesController.deleteNote);
notesRouter.put('/:id', middleware, notesController.updateNote);


module.exports = notesRouter;
