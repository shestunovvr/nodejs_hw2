require("dotenv").config({ path: "../.env" });
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

const jwtSecret = process.env.jwtSecret;

const register = async (req, res) => {
    try {
      const {username, password } = req.body;
  
      const candidate = await User.findOne({username: username});
  
      if (candidate) {
        return res.status(400).json({message: 'User already exists'});
      }

      const hashedPassword = await bcrypt.hash(password, 12);
      const user = new User({
        username: username,
        password: hashedPassword,
      });
      await user.save();
  
      res.status(200).json({message: 'Success'});
    } catch (e) {
      console.log(e);
      res.status(500).json({
        message: 'Something gone wrong, try again',
      });
    }
  };

  const login = async (req, res) => {
    try {
      const {username, password} = req.body;
  
      const user = await User.findOne({username: username});
  
      if (!user) {
        return res.status(400).json({message: 'User not found'});
      }
  
      const isMatch = await bcrypt.compare(password, user.password);
  
      if (!isMatch) {
        return res.status(400).json({message: 'Wrong password'});
      }
  
      const token = jwt.sign(
          {userId: user.id},
          jwtSecret,
          {expiresIn: 86400},
      );
  
      res.status(200).json({
          message: "Success",
          jwt_token: token
        });
    } catch (e) {
      res.status(500).json({
        message: 'Something gone wrong, try again',
      });
    }
  };

module.exports = {
    register,
    login,
};
