const Note = require("../models/note");

const createNote = async (req, res) => {
    try {
        const { text } = req.body;
        const note = new Note({
            userId: req.user._id,
            text,
        });
        await note.save();
        res.status(200).json({
            message: "Success",
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

const getNotes = async (req, res) => {
    try {
        let { offset, limit } = req.query;
        if (!offset) {
            offset = 0;
        }
        if (!limit) {
            limit = 0;
        }
        const count = await Note.count({ userId: req.user._id });
        const notes = await Note.find({ userId: req.user._id }, "-__v")
            .skip(+offset)
            .limit(+limit);

        res.status(200).json({
            offset: offset,
            limit: limit,
            count: count,
            notes,
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

const getNote = async (req, res) => {
    try {
        const note = await Note.findOne({ _id: req.params.id });
        res.status(200).json({
            note: {
                _id: note._id,
                userID: note.userId,
                completed: note.completed,
                text: note.text,
                createdDate: note.createdDate,
            },
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

const checkNote = async (req, res) => {
    try {
        const note = await Note.findOne({ _id: req.params.id });
        await Note.findByIdAndUpdate(
            { _id: req.params.id },
            { completed: !note.completed }
        );
        res.status(200).json({
            message: "Success",
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

const updateNote = async (req, res) => {
    try {
        const note = await Note.findOne({ _id: req.params.id });
        note.text = req.body.text;
        await note.save();
        res.status(200).json({
            message: "Success",
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

const deleteNote = async (req, res) => {
    try {
        await Note.deleteOne({ _id: req.params.id });
        res.status(200).json({
            message: "Success",
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

module.exports = {
    createNote,
    getNote,
    getNotes,
    checkNote,
    deleteNote,
    updateNote,
};
