const { Schema, model } = require('mongoose')

const userSchema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: new Date().toLocaleString(),
  }
});

const User = model('users', userSchema)

module.exports = User;
